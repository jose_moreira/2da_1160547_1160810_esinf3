/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import PL.AVL;
import PL.Poligono;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author jpf_m
 */
public class ReadingTreeTest {

    private static AVL<Poligono> uniTree = new AVL<>();
    private static AVL<Poligono> denTree = new AVL<>();
    private static AVL<Poligono> cenTree = new AVL<>();

    String[] ficheiro = {"src/PL/poligonos_prefixo_unidades.txt",
        "src/PL/poligonos_prefixo_dezenas.txt",
        "src/PL/poligonos_prefixo_centenas.txt"};

    private static AVL<Poligono> uniTreeTest = new AVL<>();
    private static AVL<Poligono> denTreeTest = new AVL<>();
    private static AVL<Poligono> cenTreeTest = new AVL<>();

    @Before
    public void setUp() {

        uniTreeTest.insert(new Poligono(1, "hena"));
        uniTreeTest.insert(new Poligono(2, "di"));
        uniTreeTest.insert(new Poligono(3, "tri"));
        uniTreeTest.insert(new Poligono(4, "tetra"));
        uniTreeTest.insert(new Poligono(5, "penta"));
        uniTreeTest.insert(new Poligono(6, "hexa"));
        uniTreeTest.insert(new Poligono(7, "hepta"));
        uniTreeTest.insert(new Poligono(8, "octa"));
        uniTreeTest.insert(new Poligono(9, "ennea"));

        denTreeTest.insert(new Poligono(10, "deca"));
        denTreeTest.insert(new Poligono(11, "hendeca"));
        denTreeTest.insert(new Poligono(12, "dodeca"));
        denTreeTest.insert(new Poligono(13, "triskaideca"));
        denTreeTest.insert(new Poligono(14, "tetrakaideca"));
        denTreeTest.insert(new Poligono(15, "pentakaideca"));
        denTreeTest.insert(new Poligono(16, "hexakaideca"));
        denTreeTest.insert(new Poligono(17, "heptakaideca"));
        denTreeTest.insert(new Poligono(18, "octakaideca"));
        denTreeTest.insert(new Poligono(19, "enneakaideca"));
        denTreeTest.insert(new Poligono(20, "icosa"));
        denTreeTest.insert(new Poligono(21, "icosihena"));
        denTreeTest.insert(new Poligono(22, "icosidi"));
        denTreeTest.insert(new Poligono(23, "icositri"));
        denTreeTest.insert(new Poligono(24, "icositetra"));
        denTreeTest.insert(new Poligono(25, "icosipenta"));
        denTreeTest.insert(new Poligono(26, "icosihexa"));
        denTreeTest.insert(new Poligono(27, "icosihepta"));
        denTreeTest.insert(new Poligono(28, "icosiocta"));
        denTreeTest.insert(new Poligono(29, "icosiennea"));
        denTreeTest.insert(new Poligono(30, "triaconta"));
        denTreeTest.insert(new Poligono(40, "tetraconta"));
        denTreeTest.insert(new Poligono(50, "pentaconta"));
        denTreeTest.insert(new Poligono(60, "hexaconta"));
        denTreeTest.insert(new Poligono(70, "heptaconta"));
        denTreeTest.insert(new Poligono(80, "octaconta"));
        denTreeTest.insert(new Poligono(90, "enneaconta"));

        cenTreeTest.insert(new Poligono(100, "hecta"));
        cenTreeTest.insert(new Poligono(200, "dihecta"));
        cenTreeTest.insert(new Poligono(300, "trihecta"));
        cenTreeTest.insert(new Poligono(400, "tetrahecta"));
        cenTreeTest.insert(new Poligono(500, "pentahecta"));
        cenTreeTest.insert(new Poligono(600, "hexahecta"));
        cenTreeTest.insert(new Poligono(700, "heptahecta"));
        cenTreeTest.insert(new Poligono(800, "octahecta"));
        cenTreeTest.insert(new Poligono(900, "enneahecta"));
    }

    /**
     * Test of balancedTree method, of class ReadingTree.
     */
    @Test
    public void testBalancedTree() throws Exception {
        System.out.println("balancedTree");

        uniTree = ReadingTree.lerFicheiro(ficheiro[0], uniTree);
        denTree = ReadingTree.lerFicheiro(ficheiro[1], denTree);
        cenTree = ReadingTree.lerFicheiro(ficheiro[2], cenTree);

        assertTrue(uniTree.equals(uniTreeTest));
        assertTrue(denTree.equals(denTreeTest));
        assertTrue(cenTree.equals(cenTreeTest));
    }
}
