/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import Utils.ReadingTree;
import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Utils.*;

/**
 *
 * @author jpf_m
 */
public class AlgoritmosTest {

    AVL<Poligono> uniTreeTest;
    AVL<Poligono> denTreeTest;
    AVL<Poligono> cenTreeTest;
    AVL<Poligono> arvoreTotal; 
    

    public AlgoritmosTest() throws FileNotFoundException {
        
        uniTreeTest = new AVL<>();
        denTreeTest = new AVL<>();
        cenTreeTest = new AVL<>();
        arvoreTotal = new AVL<>();
        arvoreTotal = ReadingTree.lerFicheiro("src/PL/teste_lados_nome.txt", arvoreTotal);     

        uniTreeTest.insert(new Poligono(1, "hena"));
        uniTreeTest.insert(new Poligono(2, "di"));
        uniTreeTest.insert(new Poligono(3, "tri"));
        uniTreeTest.insert(new Poligono(4, "tetra"));
        uniTreeTest.insert(new Poligono(5, "penta"));
        uniTreeTest.insert(new Poligono(6, "hexa"));
        uniTreeTest.insert(new Poligono(7, "hepta"));
        uniTreeTest.insert(new Poligono(8, "octa"));
        uniTreeTest.insert(new Poligono(9, "ennea"));

        denTreeTest.insert(new Poligono(10, "deca"));
        denTreeTest.insert(new Poligono(11, "hendeca"));
        denTreeTest.insert(new Poligono(12, "dodeca"));
        denTreeTest.insert(new Poligono(13, "triskaideca"));
        denTreeTest.insert(new Poligono(14, "tetrakaideca"));
        denTreeTest.insert(new Poligono(15, "pentakaideca"));
        denTreeTest.insert(new Poligono(16, "hexakaideca"));
        denTreeTest.insert(new Poligono(17, "heptakaideca"));
        denTreeTest.insert(new Poligono(18, "octakaideca"));
        denTreeTest.insert(new Poligono(19, "enneakaideca"));
        denTreeTest.insert(new Poligono(20, "icosa"));
        denTreeTest.insert(new Poligono(21, "icosihena"));
        denTreeTest.insert(new Poligono(22, "icosidi"));
        denTreeTest.insert(new Poligono(23, "icositri"));
        denTreeTest.insert(new Poligono(24, "icositetra"));
        denTreeTest.insert(new Poligono(25, "icosipenta"));
        denTreeTest.insert(new Poligono(26, "icosihexa"));
        denTreeTest.insert(new Poligono(27, "icosihepta"));
        denTreeTest.insert(new Poligono(28, "icosiocta"));
        denTreeTest.insert(new Poligono(29, "icosiennea"));
        denTreeTest.insert(new Poligono(30, "triaconta"));
        denTreeTest.insert(new Poligono(40, "tetraconta"));
        denTreeTest.insert(new Poligono(50, "pentaconta"));
        denTreeTest.insert(new Poligono(60, "hexaconta"));
        denTreeTest.insert(new Poligono(70, "heptaconta"));
        denTreeTest.insert(new Poligono(80, "octaconta"));
        denTreeTest.insert(new Poligono(90, "enneaconta"));

        cenTreeTest.insert(new Poligono(100, "hecta"));
        cenTreeTest.insert(new Poligono(200, "dihecta"));
        cenTreeTest.insert(new Poligono(300, "trihecta"));
        cenTreeTest.insert(new Poligono(400, "tetrahecta"));
        cenTreeTest.insert(new Poligono(500, "pentahecta"));
        cenTreeTest.insert(new Poligono(600, "hexahecta"));
        cenTreeTest.insert(new Poligono(700, "heptahecta"));
        cenTreeTest.insert(new Poligono(800, "octahecta"));
        cenTreeTest.insert(new Poligono(900, "enneahecta"));
    }


    @Test
    public void testAlgoritmos() throws FileNotFoundException {
        System.out.println("algoritmos");
        Algoritmos instance = new Algoritmos();
        assertTrue(uniTreeTest.equals(instance.getUnidades()));
        assertTrue(denTreeTest.equals(instance.getDezenas()));
        assertTrue(cenTreeTest.equals(instance.getCentenas()));
    }

    /**
     * Test of nameBuilding method, of class Algoritmos.
     */
    @Test
    public void testNameBuilding() throws FileNotFoundException {
        System.out.println("nameBuilding");
        int aux = 912;
        Algoritmos instance = new Algoritmos();
        String expResult = "enneahectadodecagon";
        String result = instance.nameBuilding(aux);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of nameBuilding method, of class Algoritmos.
     */
    @Test
    public void testNameBuilding1() throws FileNotFoundException {
        System.out.println("nameBuilding");
        int aux = 500;
        Algoritmos instance = new Algoritmos();
        String expResult = "pentahectagon";
        String result = instance.nameBuilding(aux);
        assertEquals(expResult, result);
    }
    
          /**
     * Test of nameBuilding method, of class Algoritmos.
     */
    @Test
    public void testNameBuilding2() throws FileNotFoundException {
        System.out.println("nameBuilding");
        int aux = 12;
        Algoritmos instance = new Algoritmos();
        String expResult = "dodecagon";
        String result = instance.nameBuilding(aux);
        assertEquals(expResult, result);
    }
    
        
          /**
     * Test of nameBuilding method, of class Algoritmos.
     */
    @Test
    public void testNameBuilding3() throws FileNotFoundException {
        System.out.println("nameBuilding");
        int aux = 2;
        Algoritmos instance = new Algoritmos();
        String expResult = "digon";
        String result = instance.nameBuilding(aux);
        assertEquals(expResult, result);
    }


    /**
     * Test of getunidades method, of class Algoritmos.
     */
    @Test
    public void testGetUnidades() throws FileNotFoundException {
        System.out.println("getunidades");
        Algoritmos instance = new Algoritmos();
        AVL result = instance.getUnidades();
        assertTrue(uniTreeTest.equals(result));
    }

    /**
     * Test of getdezenas method, of class Algoritmos.
     */
    @Test
    public void testGetDezenas() throws FileNotFoundException {
        System.out.println("getdezenas");
        Algoritmos instance = new Algoritmos();
        AVL result = instance.getDezenas();
        assertTrue(denTreeTest.equals(result));
    }

    /**
     * Test of getcentenas method, of class Algoritmos.
     */
    @Test
    public void testGetCentenas() throws FileNotFoundException {
        System.out.println("getcentenas");
        Algoritmos instance = new Algoritmos();
        AVL<Poligono> result = instance.getCentenas();
        assertTrue(cenTreeTest.equals(result));
    }

    /**
     * Test of adPoligono method, of class Algoritmos.
     */
    @Test
    public void testAdPoligono() throws FileNotFoundException {
        System.out.println("adPoligono");
        Algoritmos result = new Algoritmos();
        Algoritmos expResult = new Algoritmos();
        Poligono p = new Poligono(5, "pentagono");
        result.adPoligono(p);
        expResult.getUnidades().insert(p);
        System.out.println(String.format("Expected: %s", expResult));
        System.out.println(String.format("Result: %s", result));
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of arvoreBalanceada method, of class Algoritmos.
     */
    @Test
    public void testArvoreBalanceada() throws FileNotFoundException {

        System.out.println("Árvore Balanceada");
        Algoritmos instance = new Algoritmos();
        AVL<Poligono> result = instance.arvoreBalanceada();
        assertTrue(result.equals(arvoreTotal));
      }
    

    /**
     * Test of equals method, of class Algoritmos.
     */
    @Test
    public void testEquals() throws FileNotFoundException {
        System.out.println("equals");
        Object o = null;
        Algoritmos instance = new Algoritmos();
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);     
    }

    /**
     * Test of inverso method, of class Algoritmos.
     */
    @Test
    public void testInverso() throws FileNotFoundException {
        System.out.println("Nomes pela Ordem Inversa");
        int inf = 20;
        int sup = 30;
        Algoritmos instance = new Algoritmos();
        AVL<Poligono> expResult = new AVL<>();
        Poligono p1 = new Poligono(5, "pentagono");
        Poligono p2 = new Poligono(524, "pentahectaicosatetragon");
        expResult.insert(p1);
        expResult.insert(p2);
        AVL<String> result = instance.inverso(inf, sup);
        for (int i = sup; i <= inf; i--) {
            String nome = instance.nameBuilding(i);
            assertEquals(expResult, result);   
        }
    }

    /**
     * Test of numeroLados method, of class Algoritmos.
     */
    @Test
    public void testNumeroLados() throws FileNotFoundException {
        System.out.println("numeroLados");
        String poligonoNome = "pentahectaicosatetragon";
        Algoritmos instance = new Algoritmos();
        Integer expResult = 524;
        Integer result = instance.numeroLados(poligonoNome);
        assertEquals(expResult, result);
    }

    /**
     * Test of lowestCommonAncestor method, of class Algoritmos.
     */
    @Test
    public void testLowestCommonAncestor() throws FileNotFoundException {
      
        System.out.println("lowestCommonAncestor");
        Algoritmos instance = new Algoritmos();
        String poligono1 = "hexacontahexagon";
        String poligono2 = "trihectaoctacontahexagon";
        Poligono expResult = new Poligono(256, "dihectapentacontahexagon");
        Poligono result = instance.lowestCommonAncestor(poligono1, poligono2);
        assertEquals(expResult, result);
    }
}
