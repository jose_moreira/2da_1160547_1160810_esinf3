/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Eduarda Fernandes
 */
public class PoligonoTest {

    public PoligonoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getNumeroLados method, of class Poligono.
     */
    @Test
    public void testGetNumeroLados() {
        System.out.println("getNumeroLados");
        Poligono instance = new Poligono();
        int expResult = 0;
        int result = instance.getNumeroLados();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumeroLados method, of class Poligono.
     */
    @Test
    public void testSetNumeroLados() {
        System.out.println("setNumeroLados");
        int numeroLados = 5;
        Poligono instance = new Poligono();
        instance.setNumeroLados(numeroLados);
        assertTrue(numeroLados == instance.getNumeroLados());
    }

    /**
     * Test of getNome method, of class Poligono.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Poligono instance = new Poligono();
        String expResult = "sem nome (construtor)";
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNome method, of class Poligono.
     */
    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "Nome1";
        Poligono instance = new Poligono();
        instance.setNome(nome);
        assertEquals(nome, instance.getNome());
    }

    /**
     * Test of compareTo method, of class Poligono.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Poligono t = new Poligono();
        Poligono instance = new Poligono();
        int expResult = 0;
        int result = instance.compareTo(t);
        assertEquals(expResult, result);
    }


    /**
     * Test of compareTo method, of class Poligono.
     */
    @Test
    public void testCompareTo2() {
        System.out.println("compareTo");
        Poligono t = new Poligono(11, "somestuff");
        Poligono instance = new Poligono(12, "stuff");
        int expResult = -1;
        int result = instance.compareTo(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareTo method, of class Poligono.
     */
    @Test
    public void testCompareTo3() {
        System.out.println("compareTo");
        Poligono t = new Poligono(12, "somestuff");
        Poligono instance = new Poligono(10, "");
        int expResult = 1;
        int result = instance.compareTo(t);
        assertEquals(expResult, result);
    }

        /**
     * Test of compareTo method, of class Poligono.
     */
    @Test
    public void testCompareTo4() {
        System.out.println("compareTo");
        Poligono t = new Poligono(11, "");
        Poligono instance = new Poligono(11, "");
        int expResult = 0;
        int result = instance.compareTo(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Poligono.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Poligono o = new Poligono();
        Poligono instance = new Poligono();
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Poligono.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Poligono instance = new Poligono(5, "pentagono");
        String expResult = String.format("Poligono{" + "lados=" + instance.getNumeroLados() + ", prefixo=" + instance.getNome() + "}");
        String result = instance.toString();
        System.out.println(String.format("Expected: %s", expResult));
        System.out.println(String.format("Result: %s", result));
        assertEquals(expResult, result);
    }

}
