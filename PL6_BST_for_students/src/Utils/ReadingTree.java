/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import PL.AVL;
import PL.Algoritmos;
import PL.Poligono;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author jpf_m
 */
public class ReadingTree {

    private static Scanner in;

    /**
     * method to read the tree files
     *
     * @param file - file name
     * @return - filled tree
     * @throws FileNotFoundException
     */
    public static AVL<Poligono> lerFicheiro(String file, AVL<Poligono> arvore) throws FileNotFoundException {
        
        Poligono plg;
        
        in = new Scanner(new File(file));
        while (in.hasNext()) {
            String linha = in.nextLine();
            if (linha.trim().length() > 0) {
                String[] info = linha.split(";");
                plg = new Poligono(Integer.parseInt(info[0]), info[1]);
                arvore.insert(plg);
            }
        }
        return arvore;
    }
}
