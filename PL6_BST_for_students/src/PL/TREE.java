package PL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

/*
 * @author DEI-ESINF
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    /*
   * @param element A valid element within the tree
   * @return true if the element exists in tree false otherwise
     */
    public boolean contains(E element) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isLeaf(E element) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /*
   * build a list with all elements of the tree. The elements in the 
   * left subtree in ascending order and the elements in the right subtree 
   * in descending order. 
   *
   * @return    returns a list with the elements of the left subtree 
   * in ascending order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public BST<E> automnTree() {
        BST<E> newTree = new TREE();
        newTree.root = copyRec(root);

        return newTree;
    }

    public TREE<E> autumnTree() {
        TREE<E> autumn = new TREE<>();

        autumn.root = copyRec(root);
        return autumn;
    }

    private Node<E> copyRec(Node<E> node) {
        if (node.getLeft() == null) {
            return node;
        }

        if (node.getLeft() != null || node.getRight() != null) {
            return new Node(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
        }

        return null;
    }

    /**
     * @return the the number of nodes by level.
     */
    public int[] numNodesByLevel() {
        int[] result = new int[this.size()];
        int dim = height(root) + 1;
        for (int i = 0; i < dim; i++) {
            result[i] = 0;
            numNodesByLevel(root, result, 0);
        }
        return result;
    }

    private void numNodesByLevel(Node<E> node, int[] result, int level) {
        if (node == null) {
            return;
        }
        result[level] += 1;

        numNodesByLevel(node.getLeft(), result, level + 1);
        numNodesByLevel(node.getRight(), result, level + 1);
    }

    public ArrayList<E> topTree() {
        ArrayList<E> top = new ArrayList<>();

        int alt = this.height();

        topTree(root, top, alt, alt);

        return top;
    }
    
    private void topTree(Node<E> node, ArrayList<E> top, int level, int alt){
     
        if (node == null)
            return;
        
        topTree(node.getRight(), top, level +1, alt);
        if(level <= alt/2)
            top.add(node.getElement());
        
        topTree(node.getLeft(), top, level+1, alt);
    }
}
