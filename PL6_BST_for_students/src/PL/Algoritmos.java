/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import com.sun.scenario.effect.impl.Renderer;
import java.util.Objects;
import PL.AVL;
import Utils.ReadingTree;
import java.io.FileNotFoundException;

/**
 *
 * @author jpf_m
 */
public class Algoritmos {

    private AVL<Poligono> unidades = new AVL();
    private AVL<Poligono> dezenas = new AVL();
    private AVL<Poligono> centenas = new AVL();

    private static final String sufixo = "gon";

    public Algoritmos() throws FileNotFoundException {
        this.unidades = ReadingTree.lerFicheiro("src/PL/poligonos_prefixo_unidades.txt", unidades);
        this.dezenas = ReadingTree.lerFicheiro("src/PL/poligonos_prefixo_dezenas.txt", dezenas);
        this.centenas = ReadingTree.lerFicheiro("src/PL/poligonos_prefixo_centenas.txt", centenas);
    }

    public AVL<Poligono> getUnidades() {
        return unidades;
    }

    public AVL<Poligono> getDezenas() {
        return dezenas;
    }

    public AVL<Poligono> getCentenas() {
        return centenas;
    }

    public void adPoligono(Poligono p) {
        if (p.getNumeroLados() < 10) {
            unidades.insert(p);
            return;
        }
        if (p.getNumeroLados() < 100) {
            dezenas.insert(p);
            return;
        }
        if (p.getNumeroLados() < 1000) {
            centenas.insert(p);
        }
    }

    //alinea b
    public String nameBuilding(int numeroLados) {

        if (numeroLados <= 0) {
            return null;
        }
        //descobre o nome para cada "secçao" do poligono
        String nomePoligono = poligonoExistente(numeroLados, unidades);
        if (!nomePoligono.equals("")) {
            return String.format("%s" + sufixo, nomePoligono);
        }

        nomePoligono = poligonoExistente(numeroLados, dezenas);
        if (!nomePoligono.equals("")) {
            return String.format("%s" + sufixo, nomePoligono);
        }

        nomePoligono = poligonoExistente(numeroLados, centenas);
        if (!nomePoligono.equals("")) {
            return String.format("%s" + sufixo, nomePoligono);
        }

        int dezena = numeroLados / 10;
        int unidade = numeroLados % 10;
        if (dezena < 10) {
            dezena *= 10;
            return String.format("%s" + "%s" + sufixo, poligonoExistente(dezena, dezenas), poligonoExistente(unidade, unidades));
        }
        int centena = dezena / 10;
        centena *= 100;
        dezena %= 10;
        dezena *= 10;
//        nomePoligono = poligonoExistente(dezena + unidade, dezenas);
//        if (!nomePoligono.equals("")) {
//            return String.format("%s" + "%s" + sufixo, poligonoExistente(centena, centenas), nomePoligono);
//        }

        if(dezena+unidade < 20){
            dezena += unidade;
            return poligonoExistente(centena, centenas) + poligonoExistente(dezena, dezenas)
                 + sufixo;
        }
    return poligonoExistente(centena, centenas) + poligonoExistente(dezena, dezenas)
                + poligonoExistente(unidade, unidades) + sufixo;
    }

    //auxilia na "descoberta" do nome do polígono
    private String poligonoExistente(int numeroLados, AVL arvore) {
        for (Poligono p : (Iterable<Poligono>) arvore.inOrder()) {
            if (p.getNumeroLados() == numeroLados) {
                return p.getNome();
            }
        }
        return "";
    }

    //alinea c 
    public AVL<Poligono> arvoreBalanceada() {
        AVL <Poligono> arvore = new AVL();
        for (int i = 1; i < 1000; i++) {
            String nome  = nameBuilding(i);
            arvore.insert(new Poligono(i, nome));
        }
        return arvore;
    }
    
    //alinea d
    public Integer numeroLados(String poligonoNome){
        AVL arvore = arvoreBalanceada();
        for(Poligono p : (Iterable<Poligono>) arvore.inOrder()){
            if(p.getNome().equalsIgnoreCase(poligonoNome)){
                return p.getNumeroLados();
            }
        }
        return 0;
    }

    //alinea e
    public AVL<String> inverso(int inf, int sup){
        AVL<String> nomeInverso = new AVL<>();
        if( inf < 0 || sup > 99){

            return null;
        }
        if (sup < inf) {
            return null;
        }
        for(int i = sup; i < inf ; i--){
             String nome = nameBuilding(sup);
             nomeInverso.insert(nome);

        }
       return nomeInverso;
    }

    //alinea f
    public Poligono lowestCommonAncestor(String poligono1, String poligono2){
        AVL<Poligono> tree = arvoreBalanceada();
        if(poligono1 == null || poligono2 == null){
            return null;
        }
       
        int numeroLados1 = numeroLados(poligono1);
        int numeroLados2 = numeroLados(poligono2);
        
        Poligono pol1 = new Poligono(numeroLados1, poligono1);
        Poligono pol2 = new Poligono(numeroLados2, poligono2);
        
        return tree.getLowestCommonAncestor(pol1, pol2);
    }
   
    
    
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }

        final Algoritmos other = (Algoritmos) o;

        if (!Objects.equals(this.unidades, other.unidades)) {
            return false;
        }
        if (!Objects.equals(this.dezenas, other.dezenas)) {
            return false;
        }
        if (!Objects.equals(this.centenas, other.centenas)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Algoritmos: " + "unidades: " + unidades + ", dezenas: " + dezenas + ", centenas: " + centenas;
    }

}
