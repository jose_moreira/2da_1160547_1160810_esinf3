/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import java.util.Objects;

/**
 *
 * @author jpf_m
 */
public class Poligono implements Comparable<Poligono> {

    private int numeroLados;
    private String nome;

    private static final int numeroLadosDefault = 0;
    private static final String nomeDefault = "sem nome (construtor)";

    public Poligono(int numeroLados, String nome) {
        this.numeroLados = numeroLados;
        this.nome = nome;
    }

    public Poligono(int numeroLados) {
        this.numeroLados = numeroLados;
    }

    public Poligono() {
        numeroLados = numeroLadosDefault;
        nome = nomeDefault;
    }

    /**
     * @return the numeroLados
     */
    public int getNumeroLados() {
        return numeroLados;
    }

    /**
     * @param numeroLados the numeroLados to set
     */
    public void setNumeroLados(int numeroLados) {
        this.numeroLados = numeroLados;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    

    @Override
    public int compareTo(Poligono t) {
        Poligono other = (Poligono) t;
        if (other.nome.equals(this.nome) && other.numeroLados == this.numeroLados) {
            return 0;
        }
        if (!(other.nome.equals(this.nome)) && other.numeroLados < this.numeroLados) {
            return -1;
        }

        return 1;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Poligono other = (Poligono) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.numeroLados, other.numeroLados)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        return "Poligono{" + "lados=" + numeroLados + ", prefixo=" + nome + "}";
    }

}
