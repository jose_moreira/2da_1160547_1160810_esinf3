/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

/**
 *
 * @author DEI-ESINF
 * @param <E>
 */
public class AVL<E extends Comparable<E>> extends BST<E> {

    private int balanceFactor(Node<E> node) {

        return height(node.getRight()) - height(node.getLeft());
    }

    private Node<E> rightRotation(Node<E> node) {

        Node<E> leftson = node.getLeft();

        node.setLeft(leftson.getRight());
        leftson.setRight(node);

        node = leftson;
        return node;
    }

    private Node<E> leftRotation(Node<E> node) {

        Node<E> rightson = node.getRight();

        node.setRight(rightson.getLeft());
        rightson.setLeft(node);

        node = rightson;
        return node;
    }

    private Node<E> twoRotations(Node<E> node) {

        if (balanceFactor(node) < 0) {
            node.setLeft(leftRotation(node.getLeft()));
            node = rightRotation(node);
        } else {
            node.setRight(rightRotation(node.getRight()));
            node = leftRotation(node);
        }
        return node;
    }

    private Node<E> balanceNode(Node<E> node) {

        if (node == null) {
            return null;
        }
        if (balanceFactor(node) > 1) {
            if (balanceFactor(node.getRight()) < 0) {
                return twoRotations(node);
            } else {
                return leftRotation(node);
            }
        } else if (balanceFactor(node) < -1) {
            if (balanceFactor(node.getLeft()) > 0) {
                return twoRotations(node);
            } else {
                return rightRotation(node);
            }
        }
        return node;
    }

    @Override
    public void insert(E element) {
        root = insert(element, root);
    }

    private Node<E> insert(E element, Node<E> node) {
        if (node == null) {
            return new Node(element, null, null);
        }
        if (node.getElement() == element) {
            node.setElement(element);
        } else {
            if (node.getElement().compareTo(element) > 0) {
                node.setLeft(insert(element, node.getLeft()));
                node = balanceNode(node);
            } else {
                node.setRight(insert(element, node.getRight()));
                node = balanceNode(node);
            }

        }
        return node;
    }

    @Override
    public void remove(E element) {
        root = remove(element, root());
    }

    private Node<E> remove(E element, BST.Node<E> node) {
        if (node == null) {
            return null;
        }
        if (node.getElement() == element) {
            if (node.getLeft() == null && node.getRight() == null) {
                return null;
            }
            if (node.getLeft() == null) {
                return node.getRight();
            }
            if (node.getLeft() == null) {
                return node.getLeft();
            }
            E smallElement = smallestElement(node.getRight());
            node.setElement(smallElement);
            node.setRight(remove(smallElement, node.getRight()));
            node = balanceNode(node);
        } else if (node.getElement().compareTo(element) > 0) {
            node.setLeft(remove(element, node.getLeft()));
            node = balanceNode(node);
        } else {
            node.setRight(remove(element, node.getRight()));
            node = balanceNode(node);
        }
        return node;
    }
    
    public Poligono getLowestCommonAncestor(Poligono pol1, Poligono pol2){
        return lowestCommonAncestor((Node<Poligono>) root, pol1, pol2);
    }

    
    private Poligono lowestCommonAncestor(Node<Poligono> ancestor, Poligono pol1, Poligono pol2){
        if( ancestor == null){
            return null;
        }
        
        if(ancestor.getElement().getNumeroLados()<= pol1.getNumeroLados()&& ancestor.getElement().getNumeroLados()>= pol2.getNumeroLados()){
            return ancestor.getElement();
        }
        if((ancestor.getElement().getNumeroLados()>= pol1.getNumeroLados()&& ancestor.getElement().getNumeroLados()<= pol2.getNumeroLados())){
            return ancestor.getElement();
        }

        Poligono lowestCommonAncestorLeft = lowestCommonAncestor(ancestor.getLeft(), pol1, pol2);
        Poligono lowestCommonAncestorRight = lowestCommonAncestor(ancestor.getRight(), pol1, pol2);
        
        if(lowestCommonAncestorLeft != null && lowestCommonAncestorRight != null)
            return ancestor.getElement();
        
        return (lowestCommonAncestorLeft != null)? lowestCommonAncestorLeft: lowestCommonAncestorRight;
    }

    public boolean equals(AVL<E> second) {

        if (second == null) {
            return false;
        }

        if (this == second) {
            return true;
        }

        return equals(root, second.root);

    }

    public boolean equals(Node<E> root1, Node<E> root2) {

        if (root1 == root2) {
            return true;
        }
        
        if (root1 == null || root2 == null) {
            return false;
        }

        return root1.getElement().equals(root2.getElement())
                && equals(root1.getLeft(), root2.getLeft())
                && equals(root1.getRight(), root2.getRight());
    }

}
